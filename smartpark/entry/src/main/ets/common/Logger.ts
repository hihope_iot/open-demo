/**
 * 日志管理类
 */
export class logger {
    private tag: string = "SocketSample"
    private static module: string = "Log"
    private static logger: logger

    constructor(module: string) {
        logger.module = module
    }

    public static getInstance(module: any): logger {
        if (logger.module != module) {
            //获取类名作为module，方便定位日志
            this.logger = new logger(module.constructor.name)
        }
        return this.logger
    }

    debug(log: string) {
        console.debug(`[${this.tag}.${logger.module}]:${log}`)
    }

    info(log: string) {
        console.info(`[${this.tag}.${logger.module}]:${log}`)
    }

    error(log: string) {
        console.error(`[${this.tag}.${logger.module}]:${log}`)
    }
}