//export  interface DatasFace {
//        temp?: string;
//        humi?: string;
//        rt1?: number;
//        mc1?: number;
//        mc2?: number;
//      }

export class DataModel {
    temp?: string;
    humi?: string;
    rt1?: number;
    mc1?: number;
    mc2?: number;

    constructor(temp: string, humi: string, rt1: number, mc1: number, mc2:number) {
        this.temp = temp
        this.humi = humi
        this.rt1 = rt1
        this.mc1 = mc1

        this.mc2 = mc2
    }
}