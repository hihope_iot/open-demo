/*
 * Copyright (c) 2021 JianGuo Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { optionItem } from '../common/optionitem';
/**
 * 该组件为主界面第四个标签页面。
 * 该标签页显示个人设定信息界面
 * powered by jianguo
 * 2022/10/13
 */

import router from '@system.router'

@Component
export struct MeTabContent {
  private name: string = '坚果'
  private phone: string = '17752170152'
  private desc: string = '系统管理员'
  private settingItemOnClick(event: ClickEvent) {
    router.push({
      uri: 'pages/me/Setting' })
  }
  private aboutItemOnClick(event: ClickEvent) {
    router.push({
      uri: 'pages/me/About' })
  }


  build() {
    Column() {
      /**
       * 顶端的头像和昵称按钮
       * 包括：
       */
      Row() {

        Column() {
          Text(this.name)
            .fontFamily($r('sys.float.ohos_id_text_size_sub_title1'))
            .fontSize(28)
            .textOverflow({ overflow: TextOverflow.Clip })
            .maxLines(1)
            .fontWeight(FontWeight.Bold)
            .margin({ bottom: 10 })
          Text() {
            Span($r('app.string.me_id'))
              .fontFamily($r('sys.float.ohos_id_text_size_body1'))
              .fontSize(18)
              .fontColor($r('app.color.grey3'))
            Span(this.phone)
              .fontFamily($r('sys.float.ohos_id_text_size_body1'))
              .fontSize(18)
              .fontColor($r('app.color.grey3'))
          }
          .textOverflow({ overflow: TextOverflow.Ellipsis }).width(200)
          //          .maxLines(1)
          Text(this.desc)
            .fontFamily($r('sys.float.ohos_id_text_size_sub_title1'))
            .fontSize(18)
            .textOverflow({ overflow: TextOverflow.Clip })
            .maxLines(1)
            .fontColor($r('app.color.grey3'))
            .margin({ top: 10 })
        }
        .alignItems(HorizontalAlign.Start).margin(20)
        .layoutWeight(1)

        Image($r("app.media.logo"))
          .width(80)
          .height(80)
          .borderRadius(8)
          .margin(20)


      }

      .backgroundColor($r('app.color.white')).width('100%').margin(20).onClick((event) => {

      }).border({
        radius:20
      })
      /**
       * 功能列表
       */
      Column() {
        //网页版
        optionItem({ img: $r('app.media.mac'), context: $r('app.string.me_web'), isNext: true, marginTop: 10 })
        //设置
        optionItem({
          img: $r('app.media.set'),
          context: $r('app.string.me_setting'),
          isNext: true,
          itemOnClick: this.settingItemOnClick
        })
        //关于
        optionItem({
          img: $r('app.media.about'),
          context: $r('app.string.set_about'),
          isNext: true,
          itemOnClick: this.aboutItemOnClick
        })
      }.margin(10)
    }
    .width('100%').height('100%').padding({ left: 10, right: 10 })
    .backgroundColor($r('app.color.grey2'))
  }
}