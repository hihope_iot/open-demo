# 智慧园区

准备工作：将bin文件夹的bin文件烧录到3861开发板

## 使用方法：

### 1.将dayu200和3861智能家居套件供电

dayu200供电，将电源线按图连接，另一头接电源

![image-20221025142050348](https://luckly007.oss-cn-beijing.aliyuncs.com/windowsimg/image-20221025142050348.png)



3861供电：

将type—C数据线按图连接。



![image-20221025142317909](https://luckly007.oss-cn-beijing.aliyuncs.com/windowsimg/image-20221025142317909.png)

## 2.连接WiFi

用dayu200连接3861的wifi：

wifi名称：3861_car_XX:密码：12345678

![image-20221025140920457](https://luckly007.oss-cn-beijing.aliyuncs.com/windowsimg/image-20221025140920457.png)

## 3.使用



此时便可以观测在dayu200实时观测到智慧园区的温湿度变化。

启动dayu200中的智慧园区app，点击登录，无密码，此时就可以跳转到主界面。













## 铭牌信息

1. 名称：智慧园区
2. 单位：江苏润和软件股份有限公司
3. 简介：智慧园区，通过连接3861智能家居套件wifi，实现园区温湿度等监测识别，并提供OpenHarmony网关中控台，实现自动化监测，无线传感器数据采集、展示了OpenHarmony在智慧园区的应用场景。

4. OpenHarmony 3.2 beta，DAYU200开发板，Hi3861智能家居套件
